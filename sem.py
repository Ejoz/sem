import requests
import pandas as pd
from bs4 import BeautifulSoup


# Generate Instructors keywords
instructors_page = 'https://www.datacamp.com/instructors?all=true'
instructor_link_selector = '.instructor-block__description .instructor-block__link'  # CSS class of the link
instructor_name_selector = '.instructor-block__name'  # CSS class of the name

instructor_resp = requests.get(instructors_page)
soup = BeautifulSoup(instructor_resp.text, 'lxml')

instructor_urls = [url['href'] for url in soup.select(instructor_link_selector)]
instructor_names = [name.text.strip() for name in soup.select(instructor_name_selector)]
instructor_urls = ['https://www.datacamp.com' + url for url in instructor_urls]

instructor_df = pd.DataFrame({
    'name': instructor_names,
    'url': instructor_urls
})


# Creation of a template for the campaign
def generate_keywords(topics, keywords, campaign, match_types=['Exact', 'Phrase', 'Broad']):
    col_names = ['Campaign', 'Ad Group', 'Keyword', 'Criterion Type']
    campaign_keywords = []

    for topic in topics:
        for word in keywords:
            for match in match_types:
                if match == 'Broad':
                    keyword = '+' + ' +'.join([topic.lower().replace(' ', ' +'), word.replace(' ', ' +')])
                else:
                    keyword = topic.lower() + ' ' + word
                row = [campaign,  # campaign name
                       topic,  # ad group name
                       keyword,  # instructor <keyword>
                       match]  # keyword match type
                campaign_keywords.append(row)

    for topic in topics:
        for word in keywords:
            for match in match_types:
                if match == 'Broad':
                    keyword = '+' + ' +'.join([word.replace(' ', ' +'), topic.lower().replace(' ', ' +')])
                else:
                    keyword = word + ' ' + topic.lower()
                row = [campaign,  # campaign name
                       topic,  # ad group name
                       keyword,  # <keyword> instructor
                       match]  # keyword match type
                campaign_keywords.append(row)

    return pd.DataFrame.from_records(campaign_keywords, columns=col_names)


# Instructors keywords
instructors_names = instructor_df['name']
instructors_keywords = ['course', 'courses', 'learn', 'data science', 'data camp', 'datacamp']
instructors_keywords_df = generate_keywords(instructor_names, instructors_keywords, campaign='SEM_Instructors')

# Tech keywords
tech_topics = ['R', 'Python', 'SQL', 'Git', 'Shell']  # listed on the /courses page
tech_keywords = ['data science', 'programming', 'analytics', 'data analysis', 'machine learning',
                 'deep learning', 'financial analysis', 'data viz', 'visualization', 'data visualization',
                 'learn', 'course', 'courses', 'education', 'data import', 'data cleaning',
                 'data manipulation', 'probability', 'stats', 'statistics', 'course', 'courses',
                 'learn', 'education', 'tutorial']
tech_keywords_df = generate_keywords(tech_topics, tech_keywords, campaign='SEM_Technologies')

# Full df
keywords_df = pd.concat([instructors_keywords_df, tech_keywords_df])
print('Total Keywords: ', keywords_df.shape[0])
print('Total Campaigns: ', len(set(keywords_df['Campaign'])))
print('Total Ad groups: ', len(set(keywords_df['Ad Group'])))
keywords_df.to_csv('keywords.csv', index=False)
